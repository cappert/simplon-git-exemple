# Utilisation basique de git

## Entièrement en ligne de commande, pour plus de fun

1. Créer un dossier first-git, puis aller dedans et initialiser un dépôt git avec la commande git init
2. Rajouter un nouveau fichier dans ce dossier avec une phrase, peu importe quoi, dedans. Faire un git status pour voir l'état du git
3. Rajouter le fichier en staging avec un git add (comme suggéré par le git status), puis refare un git status pour voir l'évolution
4. Ensuite, on va stocker cette version du fichier dans le dépôt en utilisant git commit -m "mon message" en remplaçant "mon message" par un truc plus pertinent
5. Modifier ou rajouter du texte dans le fichier, puis créer un autre fichier avec une autre phrase dedans puis faire en sorte de commit uniquement le second fichier
6. Modifier le contenu du second fichier et faire en sorte de commit les modification et du premier et du second fichier
7.  Utiliser git log pour voir l'historique des commit, vous verrez un identifiant de ce genre là `ee1c7c72fe46358f8064fd156cdb1a0f1dccc5ad` à côté de chaque commit
8. Copier l'identifiant du premier commit, et l'utiliser à la suite d'un git checkout pour remettre les fichiers dans l'état du premier commit (vous pouvez vérifier avec un ls et un cat pour voir ce qu'il y a comme fichier)
9. Pour revenir à la version actuelle de votre code, faire un git checkout master 
Au premier commit, il vous demandera de configurer git, copier-coller les commandes indiqué par git en remplaçant le nom d'exemple par votre username gitlab (ou votre nom/prénom, peu importe), et en remplaçant l'email par l'email que vous avez/allez utilisé pour créer votre compte gitlab

## Configuration gitlab, créez une clef SSH : 

Si vous n'avez pas de compte, crééz le
* ouvrir un nouveau terminal et exécuter `ssh-keygen -t ed25519 -C "gitlab"` puis faire entrée jusqu'à ce qu'il arrête de demander des trucs
* afficher le contenu de votre clef publique avec `cat ~/.ssh/id_ed25519.pub` et copier le résultat
* Aller sur https://gitlab.com/-/profile/keys et coller la clef publique que vous venez de copier et valider 
Suite de l'exercice, mettre le code en ligne :
10. Aller sur gitlab.com et créer un nouveau repository vide, sans README
11. Cliquer sur le bouton cloner et copier le lien ssh
12. Sur votre terminal, dans le dossier de votre dépôt git, faire un git remote add origin url en remplaçant url par le lien ssh que vous venez de copier
13. Faire un git push origin master pour mettre votre projet en ligne, et aller constater que ça a marché, ou pas sur gitlab.com

## Bonus, pistes d'exploration :

* Modifier un fichier directement sur l'interface de gitlab et le commit, puis faire une modification sur sa machine en ligne de commande et essayer de la commit/push
* Supprimer le dossier sur sa machine et essayer de le récupérer depuis gitlab avec un git clone
* Explorer les commit et l'historique des fichiers sur l'interface de gitlab 


Pour configurer la branche par défaut, exécuter :
`git config --global init.defaultBranch main`
